require 'set'

class Node
  attr_accessor :name, :neighbors
  def initialize name
    @name = name
    @neighbors = Hash.new
  end

  def connect_to(node, connection)
    neighbors[node] = Array.new unless neighbors[node]
    neighbors[node] << connection
  end

  def to_s
    name
  end
end

class Graph
  class BacktraceNode < Node
    attr_accessor :pre, :cost

    def initialize name
      super name
      @cost = 0
    end

  end

  attr_accessor :nodes

  def initialize
    @nodes = Hash.new
  end

  def add_node name
    @nodes[name] = BacktraceNode.new name unless @nodes[name]
    @nodes[name]
  end

  def path src, dest, only_first=true
    src = nodes[src]
    dest = nodes[dest]
    if only_first
      path_node_first src, dest
    else
      path_node_complete src, dest
    end
  end

  def path_node_first src, dest
    close_set = Set.new
    queue = [src]
    close_set << src
    find = false
    src.cost = 0
    src.pre = nil
    dest.pre = []
    loop_counter = 0
    while not queue.empty?
      current = queue.shift
      newcost = current.cost + 1
      loop_counter += 1
      if current.neighbors.include? dest
        find = true
        dest.cost = newcost
        dest.pre << current
        break
      end
      current.neighbors.keys.each do |n|
        next if close_set.include? n
        close_set << n
        n.cost = newcost
        queue << n
        n.pre = [current]
      end
    end
    p "total evaltion in while loop is #{loop_counter}"
    if find
      backtrace src, dest
    else
      []
    end
  end

  def path_node_complete src, dest
    close_set = Set.new
    find = false
    queue = [src]
    queue_set = Set.new
    src.cost = 0
    src.pre = nil
    dest.pre = []
    loop_counter = 0
    while not queue.empty?
      current = queue.shift
      queue_set.delete current
      close_set << current
      newcost = current.cost + 1
      loop_counter += 1
      if find and newcost > dest.cost
        break
      end
      if current.neighbors.include? dest
        dest.cost = newcost
        dest.pre << current
        # find the first solution
        find = true
      end
      current.neighbors.keys.each do |n|
        next if close_set.include? n or n == dest
        if queue_set.include? n
          if newcost == n.cost
            n.pre << current
          end
        else
          n.cost = newcost
          queue << n
          queue_set << n
          n.pre = [current]
        end
      end
    end
    p "total evaltion in while loop is #{loop_counter}"
    if find
      return backtrace src, dest
    end
    []
  end

  def backtrace src, dest
    print_tree = lambda do |dest|
      if dest.pre
        result = dest.pre.reduce [] {|init, e| init.concat (print_tree.call e)}
        result.map do |array|
          array.push array.last.neighbors[dest]
          array.push dest
        end
      else
        [[dest]]
      end
    end
    print_tree.call dest
  end

end

$books = Hash.new
graph = Graph.new
t1 = Time.now
# File.open 'input.tsv', 'r' do |f|
File.open './labeled_edges.tsv', 'r' do |f|
  while line = f.gets
    line.chomp!
    line.gsub! /"/, ''
    character, book = line.split("\t")
    unless $books[book]
      $books[book] = Set.new
    end
    $books[book] << (graph.add_node character)
  end
end

$books.each do |book, characters|
  list_characters = characters.to_a
  list_characters.each_with_index do |c, index|
    list_characters.each do |other|
      c.connect_to other, book if c != other
    end
  end
end
t2 = Time.now

p "finished constructing graph using #{t2-t1} seconds"
p "#nodes is #{graph.nodes.size}"
sum = 0
graph.nodes.values.each do |n|
  sum += n.neighbors.size
end
p "#edges is #{sum}"

def verify_path solution
  solution = solution.clone
  lower = solution.shift
  while solution.size >= 2
    upper = lower
    appear_books = solution.shift
    lower = solution.shift
    appear_books.each do |book|
      p "#{upper} doesnt appear in #{book}" unless $books[book].include? upper
      p "#{lower} doesnt appear in #{book}" unless $books[book].include? lower
    end
  end
end
p "Auto Testing"

# for i in 0..9
for i in 0..0
  # src, dest = graph.nodes.keys.sample(2)
  # src = "MISS AMERICA/MADELIN"
  # dest = "BYREL"
  src = "A"
  dest = "G'RATH"
  p src, dest
  t1 = Time.now
  solution = graph.path src, dest
  t2 = Time.now
  p "searching first solution takes #{t2 - t1} seconds"
  solution.each do |s|
    p s
    p s.last.cost
    verify_path s
  end

  t1 = Time.now
  solution = graph.path src, dest, false
  t2 = Time.now
  p "searching all the solutions takes #{t2 - t1} seconds"
  solution.each do |s|
    p s
    p s.last.cost
    verify_path s
  end
end
