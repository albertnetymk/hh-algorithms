# require 'pry'

matrix = Hash.new
File.open './BLOSUM50.txt', 'r' do |f|
  header = f.gets
  header.chomp!

  headers = header.split ' '
  while line = f.gets
    line.chomp!
    list = line.split(" ")
    key = list.shift
    matrix[key] = Hash.new
    list.each_with_index do |value, index|
      matrix[key][headers[index]] = Integer(value)
    end
    # p key
    # p matrix[key]
  end
end

def prepare_dp a, b
  dp = Array.new (a.length+1) { Array.new (b.length + 1) {""} }
  direction = Array.new (a.length+1) { Array.new (b.length + 1) {} }
  # space match space
  dp[0][0] = 0

  # first row
  dp[0][1] = -2
  # consecutive space
  (1..(b.length-1)).each do |i|
    dp[0][i+1] = dp[0][i] + -8
    direction[0][i] = -1
  end
  direction[0][b.length] = -1

  # first column
  dp[1][0] = -2
  # consecutive space
  (1..(a.length-1)).each do |i|
    dp[i+1][0] = dp[i][0] + -8
    direction[i][0] = 1
  end
  direction[a.length][0] = 1

  return dp, direction
end

def align_sequence a, b, matrix
  p a
  p b
  dp, direction = prepare_dp a, b

  # populate dynamic programming table
  (0..(a.length-1)).each do |i|
    (0..(b.length-1)).each do |j|
      upper = dp[i][j+1] + -2
      upper += -6 if direction[i][j+1] == 1

      left = dp[i+1][j] + -2
      left += -6 if direction[i+1][j] == -1

      diagnal = dp[i][j] + matrix[a[i]][b[j]]
      max = [upper, left, diagnal].max
      dp[i+1][j+1] = max

      case max
      when diagnal
        direction[i+1][j+1] = 0
      when upper
        direction[i+1][j+1] = 1
      when left
        direction[i+1][j+1] = -1
      end
    end
  end

  show_2d_array dp
  show_2d_array direction
  show_alignment a, b, matrix, dp, direction
end

def show_alignment a, b, matrix, dp, direction
  align_a = ''
  align_b = ''
  i, j = a.length-1, b.length-1
  while i >= 0 or j >= 0
    case direction[i+1][j+1]
    when 1
      align_a.prepend a[i]
      align_b.prepend '_'
      i -= 1
    when -1
      align_a.prepend '_'
      align_b.prepend b[j]
      j -= 1
    when 0
      align_a.prepend a[i]
      align_b.prepend b[j]
      i -= 1
      j -= 1
    end
  end
  p align_a
  p align_b
end

def show_2d_array m
  m.each do |row|
    row.each do |x|
      print x.to_s.rjust(5)
    end
    puts
  end
end

def remove_start_star str
  str[0] = '' if str[0] == '*'
  str
end

def remove_end_star str
  str[-1] = '' if str[-1] == '*'
  str
end

keys = matrix.keys

a = Array.new(8) { keys.sample }.join
b = Array.new(8) { keys.sample }.join

a = "GAATTCAGTTA"
# b = "AATTAGTTA"
# a = "GGATCGA"
b = "GGATCGA"

remove_start_star a
remove_start_star b
remove_end_star a
remove_end_star b

align_sequence a, b, matrix
