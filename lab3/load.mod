# jobs, [index, duration]
set J, dimen 2;
set M, dimen 1;

set I := setof {(i,t) in J} i;

var x{I, M}, binary;

var L;

minimize obj: L;

s.t. total {(i,t) in J}: sum{m in M} x[i,m] * t = t;
s.t. load {m in M}: sum{(i,t) in J} x[i,m] * t <=L;

solve;

printf "machine_index: job_index job_duration\n";
printf {m in M, (i,t) in J: x[i,m] == 1} "%i: %i %i\n", m, i, t;

data;

set J :=
  1 10
  2 15
  3 20
  4 30;

set M :=
  1
  2;

end;
