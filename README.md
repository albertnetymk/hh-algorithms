# Algorithms 2013-11 -- 2013-12

Final course in my master program. I don't like C++ that much, so I implement all the labs in ruby.

## lab1

* Breadth First Search in graph
* Unique nodes in graph

## lab2
I am not good at GUI programming, so only hello world GUI toy in ruby.

## lab3

* Sequence alignment using dynamic programming.
* Modeling load balancing using GLPK. (`glpsov --math load.mod`)
